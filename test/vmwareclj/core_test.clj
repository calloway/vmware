(ns vmwareclj.core-test
  (:require [clojure.test :refer :all]
            [vmwareclj.core :refer :all]))

(deftest empty-flip-test
  (testing "testing empty flip"
    (is (= "" (flip "")))))
  
(deftest flip-test
  (testing "testing simple flip"
    (is (= "1" (flip "0"))))
  (testing "testing simple flip back"
    (is (= "0" (flip "1"))))
  (testing "testing flip"
    (is (= "0110" (flip "1001")))))

(deftest flip-test-with-index
  (testing "testing simple flip"
    (is (= "00" (flip "10" 0 0))))
  (testing "testing simple flip"
    (is (= "111" (flip "101" 1 1))))
  (testing "testing entire flip"
    (is (= "01011" (flip "10100" 0 4)))))


(deftest flip-longer-then-30
  (testing "testing simple flip"
    (is (= "1111111111111111111011111111111111110010000000000000000001000000000000000000"
           (flip "1000000000000000000100000000000000000010000000000000000001000000000000000000" 1 35)))))

(deftest count1s-test
 (testing "simple count 1's"
    (is (= 5 (count1s "111110000")))))

(deftest find-max-simple-test
   (testing "simple finding max"
     (is (= '([1 5 "11101110"] [1 7 "11101101"])
            (:result (findmax "10010010" 8))))))
