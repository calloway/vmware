(ns vmwareclj.core
  (:require [clojure.java.io :as io]
            [clojure.string :refer [split-lines blank? trim replace]]))

;;; x is "1001" --> "0110"
;;; (flip "1001" 1 2 --> "1111"
;;; 0 indexed.
;;; L and R are left and right.  flips inclusively

(defn flip
  ([x] (replace (replace (replace x #"1" "x") #"0" "1") #"x" "0"))              
  ([input L R]
     (str (apply str (take L input)) (flip (subs input L (inc R))) (apply str (take-last (dec (- (count input) R)) input)))))

(defn count1s [x]
  (count (filter #(= \1 %) (seq x))))

(defn findmax [input len]
  (loop [result input L 0 R L max (count1s input) acc {:max max :result [[-1 -1 input]]}]   
    (cond (>= L (count input))
          acc
          (= R (count input))
          (recur result (inc L) (inc L) max acc)
          :else
          (let [flipped (flip input L R) flippedcountls (count1s flipped)]
            (cond
             (> flippedcountls max)
             (recur flipped L (inc R) flippedcountls {:max flippedcountls :result [[L R flipped]]})
             (>= flippedcountls max)
             (recur flipped L (inc R) flippedcountls (-> acc (assoc :result (-> (:result acc) (concat [[L R flipped]])))))
             :else
             (recur result L (inc R) max acc))))))

(defn -main
  "I don't do a whole lot."
  [& args]
  (let [lines (split-lines (slurp (first args)))
        N (read-string ( first lines))         
        inputstr (apply str (filter #(or (= \0 %) (= \1 %)) (second lines)))]
    (println (findmax inputstr N))))

;;; first attempt
;;;
;;;
(defn firstattemptflip ([x]
              (if (blank? x) x
                  (let [len (count x)
                        xbin (Integer/parseInt x 2)
                        flipped ((fn[x len] (loop [i 0 acc x] 
                                             (if (= i len)
                                               acc
                                               (recur (inc i) (bit-flip acc i))))) xbin len)
                        flippedstr (Integer/toString flipped 2)
                        padding (apply str (repeat (- len (count flippedstr)) "0"))]
                    (str padding flippedstr))))
  ([input L R]
     (if (< (- R L) 30)       
       (str (apply str (take L input)) (flip (subs input L (inc R))) (apply str (take-last (dec (- (count input) R)) input)))
       (loop [i L j (+ L 30) acc (apply str (take L input)) ]
         (if (> j R)
           (apply str (concat acc (flip (subs input i (inc R))) (apply str (take-last (dec (- (count input) R)) input))))
           (recur (+ i 30) (+ j 30) (concat acc (flip (subs input i j)))))))))
